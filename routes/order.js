const express = require('express')
const router = express.Router()
const orderModel = require('../model/order')

////불러오기
router.get('/', (req, res) => {
    orderModel
        .find()
        .populate("product",["name","price"])
        .then(docs => {
            const response = {
                count : docs.length,
                products : docs.map(doc => {
                    return{
                        id : doc._id,
                        product : doc.product,
                        quantity : doc.quantity,
                        request : {
                            type : "GET",
                            url : "http://localhost:3030/order/" + doc._id
                        }
                    }
                })   
            }
            res.json(response)
        })
        .catch(err =>{
            res.json({
                message : err.message
            })
        })
})


////1개불러오기
router.get('/:orderid', (req, res) => {
    orderModel
        .findById(req.params.orderid)
        .then(doc => {
            res.json({
                id : doc._id,
                product : doc.product,
                quantity : doc.quantity,
                request : {
                    type : "GET",
                    url : "http://localhost:3030/order"
                }
            })
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
})


////등록하기
router.post('/', (req, res) => {
    const order = new orderModel({
        product : req.body.productid,
        quantity : req.body.qty
    })

    order
        .save()
        .then(doc => {
            res.json({
                orderInfo : {
                id : doc._id,
                product : doc.product,
                quantity : doc.quantity,
                request : {
                    type : "GET",
                    url : "http://localhost:3030/order"
                    }
                }
            })
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        }) 
})


////수정하기
router.patch('/:orderid', (req, res) => {
    const updateOps = {}
    for (const ops of req.body) {
        updateOps[ops.propName] = ops.value
    }

    orderModel
    .findByIdAndUpdate(req.params.orderid ,{ $set : updateOps})
    .then(() => {
        res.json({
            message : "update" ,
            request : {
                type : "GET",
                url : "http://localhost:3030/order/" + req.params.orderid
                }
            })
        })
    .catch(err => {
        res.json({
            message : err.message
        })
    })
})


////삭제하기
router.delete('/:order', (req, res) => {
    orderModel
        .findByIdAndDelete(req.params.orderid)
        .then(() => {
            res.json({
                message : "delete",
                request : {
                    type : "GET",
                    url : "http://localhost:3030/order"
                }
            }) 
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
})


module.exports = router