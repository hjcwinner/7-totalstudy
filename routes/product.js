const express = require('express')
const router = express.Router()
const productModel = require('../model/product')


////불러오기
router.get('/', (req, res) => {
    productModel
        .find()
        .then(docs => {
            const response = {
                count : docs.length,
                products : docs.map(doc => {
                    return{
                        id : doc._id,
                        product : doc.name,
                        price : doc.price,
                        request : {
                            type : "GET",
                            url : "http://localhost:3030/product/" + doc._id
                        }
                    }
                })   
            }
            res.json(response)
        })
        .catch(err =>{
            res.json({
                message : err.message
            })
        })
})


////1개불러오기
router.get('/:productid', (req, res) => {
    productModel
        .findById(req.params.productid)
        .then(doc => {
            res.json({
                id : doc._id,
                name : doc.name,
                price : doc.price,
                request : {
                    type : "GET",
                    url : "http://localhost:3030/product"
                }
            })
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })

})


////등록하기
router.post('/', (req, res) => {
    const product = new productModel({
        name : req.body.tname,
        price : req.body.tprice
    })

    product
        .save()
        .then(doc => {
            res.json({
                id : doc._id,
                name : doc.name,
                price : doc.price,
                request : {
                    type : "GET",
                    url : "http://localhost:3030/product"
                }
            })
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })


})


////수정하기
router.patch('/:productid', (req, res) => {
    const updateOps = {}
    for (const ops of req.body) {
        updateOps[ops.propName] = ops.value
    }

    productModel
    .findByIdAndUpdate(req.params.productid ,{ $set : updateOps})
    .then(() => {
        res.json({
            message : "update" ,
            request : {
                type : "GET",
                url : "http://localhost:3030/product/" + req.params.productid
                }
            })
        })
    .catch(err => {
        res.json({
            message : err.message
        })
    })
})


////삭제하기
router.delete('/:productid', (req, res) => {
    productModel
        .findByIdAndDelete(req.params.productid)
        .then(() => {
            res.json({
                message : "delete",
                request : {
                    type : "GET",
                    url : "http://localhost:3030/product"
                }
            }) 
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
})

module.exports = router