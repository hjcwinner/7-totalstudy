const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const morgan = require('morgan')
const dotEnv = require('dotenv')
dotEnv.config()

const productRoutes = require('./routes/product')
const orderRoutes = require('./routes/order.js')
const userRoutes = require('./routes/user')



////middleware
app.use(morgan("dev"))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended : false}))


app.use('/product', productRoutes)
app.use('/order', orderRoutes)
app.use('/user', userRoutes)


const port = process.env.PORT
app.listen(port, console.log("서버시작"))